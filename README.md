# Example of prometheus, grafana and python connexion app

## Prepare docker images
```
docker build -t myapp/prometheus:v1.0.0 ./prometheus
docker build -t myapp/api:v1.0.0 ./myapp
docker build -t myapp/grafana:v1.0.0 ./grafana
```

## Merge compose files
```
docker-compose \
    -f compose/docker-compose.yml \
    -f compose/docker-compose-dev.yml config > compose/myapp-compose.yml
```

## Compose your app
```
docker-compose -f compose/myapp-compose.yml up -d
```

## Open your app in browser
* [api](http://localhost:8080/ui)
* [api metrics](http://localhost:8081)
* [grafana](http://localhost:3000)
* [prometheus](http://localhost:9090)
