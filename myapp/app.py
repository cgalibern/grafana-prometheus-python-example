import connexion
import logging
import prometheus_client
import time
import uuid

#from myapp.api.loglib import getLogger

log_main = logging.getLogger(__name__)
log_api = logging.getLogger(__name__)


def before_request():
    connexion.request.begin_time = time.time()
    connexion.request.uuid = str(uuid.uuid4())
    log_api.info('starting request')


def after_request(response):
    latency = time.time() - connexion.request.begin_time
    log_api.info('ending request', extra={'latency':latency,
                                          'status_code': response.status_code})
    return response


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(message)s')
    app = connexion.FlaskApp(__name__, specification_dir='api/swagger/')
    app.app.before_request(before_request)
    app.app.after_request(after_request)
    log_main.info('starting prometheus server')
    prometheus_client.start_http_server(8081)
    app.add_api('my_api.yaml', base_path='/v1')
    log_main.info('starting api server')
    app.run(port=8080)
