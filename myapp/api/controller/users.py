# from myapp.api.loglib.mylog import getLogger
from logging import getLogger

import time

from ..prometheus_instruments import api_call_time

USERS = ['user1']


get_user_summary = api_call_time.labels(controller='users',
                                        function='get_users1')
post_user_summary = api_call_time.labels(controller='users',
                                         function='post_user1')
log = getLogger(__name__)


@get_user_summary.time()
def get_users():
    time.sleep(0.3)
    log.info('returning users', extra={'users': USERS})
    return USERS


@post_user_summary.time()
def post_user(body):
    name = body['name']
    log.info('creating user', extra={"body": body})
    time.sleep(0.4)
    USERS.append(name)
    return {'name': name}
