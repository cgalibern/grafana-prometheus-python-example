from prometheus_client import Counter, Summary


api_calls = Counter('application_api_calls', 'api controller function calls', ['controller', 'function'])
api_call_time = Summary('application_processing', 'Time spent processing request', ['controller', 'function'])
